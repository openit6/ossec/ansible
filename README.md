# PROJECT OSSEC

## Requirements

On Kali machines, need to install manually ssh and python3 and enable and start ssh service in order to use ansible

## Ansible

dynamic inventory <https://docs.ansible.com/ansible/latest/collections/community/general/proxmox_inventory.html#community-general-proxmox-inventory-proxmox-inventory-source>

monitoring stack collection: <https://galaxy.ansible.com/fahcsim/grafana_stack> install: `ansible-galaxy collection install fahcsim.grafana_stack`

Before trying to use terraform, duplicate the file `source_env.template`, remove the extension and fill the missing values. Then use the command:
`source source_env` to set the credentials as environnement variables

## TODO

* Update README with hosts instructions
* code all needed playbooks
* Update documentation
